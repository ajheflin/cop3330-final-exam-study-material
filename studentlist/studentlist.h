//
// Created by AJ Heflin on 2019-12-10.
//
#include <string>
using namespace std;

#ifndef MEGAPROJECTSTUDENTLIST_STUDENTLIST_H
#define MEGAPROJECTSTUDENTLIST_STUDENTLIST_H

class Student {
public:
    Student(string fname1, string lname1, double grade1);
    string GetFullNameGrade();
private:
    string fname;
    string lname;
    double grade;
};

class StudentList {
public:
    StudentList();
    ~StudentList();
    void AddStudent(Student);
    void PrintList();
private:
    Student** roster;
    int size;
};

#endif //MEGAPROJECTSTUDENTLIST_STUDENTLIST_H
