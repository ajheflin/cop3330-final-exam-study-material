#include <iostream>
#include <string>
#include "studentlist.h"

using namespace std;

Student::Student(string fname1, string lname1, double grade1) {
    fname = fname1;
    lname = lname1;
    grade = grade1;
}
string Student::GetFullNameGrade() {
    return fname + " " + lname + " -> " + to_string(grade);
}

StudentList::StudentList() {
    size = 0;
    roster = new Student*[1];
}
StudentList::~StudentList() {
    //Deallocate all data and reset size when object goes out of scope.
    delete [] roster;
    size = 0;
}
void StudentList::AddStudent(Student s1) {
    if(size == 0) {
        roster[0] = &s1;
        size++;
    } else {
        Student** rosterCopy = new Student*[size + 1];
        for(int i = 0; i < size; i++) {
            rosterCopy[i] = roster[i];
        }
        size++;
        rosterCopy[size - 1] = &s1;
        delete [] roster;
        roster = rosterCopy;
    }
}
void StudentList::PrintList() {
    for(int i = 0; i < size; i++) {
        cout << roster[i]->GetFullNameGrade() << endl;
    }
}


int main() {
    //Let's make a studentlist.
    StudentList l1;
    Student s1("Adam", "Heflin", 99.99);
    l1.AddStudent(s1);
    Student s2("Logan", "McClain", 95.50);
    l1.AddStudent(s2);
    l1.PrintList();
    return 0;
}