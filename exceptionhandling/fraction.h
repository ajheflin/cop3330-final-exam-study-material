//
// Created by AJ Heflin on 2019-12-10.
//
#include <exception>
using namespace std;

#ifndef MEGAPROGRAMTHROWCATCH_FRACTION_H
#define MEGAPROGRAMTHROWCATCH_FRACTION_H

class Fraction {
private:
    int numerator;
    int denominator;
public:
    Fraction();
    Fraction(int, int);
    double Evaluate();
};

#endif //MEGAPROGRAMTHROWCATCH_FRACTION_H
