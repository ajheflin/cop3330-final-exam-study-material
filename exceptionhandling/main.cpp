#include <iostream>
#include "fraction.h"
#include <exception>

using namespace std;

Fraction::Fraction() {
    numerator = 0;
    denominator = 1;
}
Fraction::Fraction(int num, int denom) {
    numerator = num;
    denominator = denom;
}
struct DivideByZero {

};

double Fraction::Evaluate() {
    return static_cast<double>(numerator) / static_cast<double>(denominator);
    try {
        if(denominator == 0) {
            DivideByZero num;
            throw num;
        } else {
            return static_cast<double>(numerator) / static_cast<double>(denominator);
        }
    }
    catch(DivideByZero) {
        cout << "Denominator is zero!" << endl;
        return 0;
    }
}

int main()
{
    Fraction f1(5, 0);
    double f1Val = f1.Evaluate();
    cout << f1Val;
    return 0;
}