#include <iostream>
#include <cstring>
#include "flex.h"
using namespace std;

ostream& operator<< (ostream& s, const Flex& f)
{
    s << '*' << f.str << '*';
    return s;
}

Flex::Flex()
{
    size = 1;			// size doesn't include null char
    str = new char[size+1];	// allocate +1 for null char
    strcpy(str," ");
}

Flex::Flex(const char * s)
{
    size = strlen(s);
    str = new char[size+1];
    strcpy(str,s);
}

Flex::~Flex()
// not specifically required by the specs, but a good idea to have this
{
    delete [] str;
}

void Flex::cat(const Flex & f)
// this function can also be made easier through the use of the
// strcat library function for concatenating strings.
// dyanamic reallocation still required, though.
{
    int newsize = size + strlen(f.str);

    char * temp = new char[newsize+1];	// allocate with room for '\0'
    strcpy(temp,str);			// copy this string to temp
    for (int i = 0; i <= f.size; i++)
        temp[size+i] = f.str[i];	// concatenate f.str to temp,
    //   including '\0'

    delete [] str;			// delete old array
    str = temp;				// set str to new one
    size = newsize;			// update size tracker
}


int main()
{
    Flex a, b("Merry"), c("Christmas");
    cout << a << ',' << b << ',' << c << endl;
    b.cat(a);
    cout << b << endl;
    b.cat(c);
    cout << b << endl;
    c.cat(c);
    c.cat(c);
    cout << c << endl;
    return 0;
    /*
	* *,*Merry*,*Christmas*
	*Merry *
	*Merry Christmas*
	*ChristmasChristmasChristmasChristmas*
     */
}