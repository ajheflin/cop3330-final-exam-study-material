#include <iostream>
using namespace std;

class Flex
{
    friend ostream& operator<< (ostream& s, const Flex& f);

public:
    Flex();			// default constructor
    Flex(const char *);		// constructor with parameters
    ~Flex();			// destructor (not specifically required)
    void cat(const Flex & f);	// cat function -- concatenation

private:
    char * str;			// variable length string
    int size;
};