cmake_minimum_required(VERSION 3.13)
project(codewriting)

set(CMAKE_CXX_STANDARD 14)

add_executable(codewriting main.cpp)