cmake_minimum_required(VERSION 3.13)
project(megaprogram)

set(CMAKE_CXX_STANDARD 14)

add_executable(megaprogram main.cpp)