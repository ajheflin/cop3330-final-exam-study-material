//
// Created by AJ Heflin on 2019-12-10.
//

#ifndef MEGAPROGRAM_CIRCLE_H
#define MEGAPROGRAM_CIRCLE_H

class Circle {
public:
    void SetCenter(int x, int y);
    void SetRadius(int r);
    void Draw();
    double AreaOf() const;
private:
    int radius;
    int center_x;
    int center_y;
};

#endif //MEGAPROGRAM_CIRCLE_H
