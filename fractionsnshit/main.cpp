#include <iostream>
#include "circle.h"
#include "fraction.h"
#include <math.h>

using namespace std;

//Utility functions
int gcd(int a, int b)
{
    // Everything divides 0
    if (a == 0)
        return b;
    if (b == 0)
        return a;

    // base case
    if (a == b)
        return a;

    // a is greater
    if (a > b)
        return gcd(a-b, b);
    return gcd(a, b-a);
}

//Circle Definitions

void Circle::SetCenter(int x, int y) {
    center_x = x;
    center_y = y;
}
void Circle::SetRadius(int r) {
    radius = r;
}
void Draw() {
    //yeah fuck drawing it I got other things to focus on.
}
double Circle::AreaOf() const {
    return M_PI * radius * radius;
}

Fraction::Fraction() {
    numerator = 0;
    denominator = 1;
}
Fraction::Fraction(int num, int denom) {
    numerator = num;
    denominator = denom;
}
Fraction::Fraction(int n) {
    numerator = n;
    denominator = 1;
}
double Fraction::Evaluate() {
    cout << static_cast<double>(numerator) / static_cast<double>(denominator);
}
void Fraction::Simplify() {
    int divisor = gcd(numerator, denominator);
    numerator = numerator / divisor;
    denominator = denominator / divisor;
}
//Let's overload the << operator for the fraction class.

ostream& operator<<(ostream& s, Fraction f1) {
    //we gotta friend this in the fraction.h class, check that for syntax.
    s << f1.numerator << " / " << f1.denominator;
    return s;
}

//That was fun, let's overload some more shit.

//Overloading ++Fraction.

//Once again, we have to friend this in the header file.
Fraction operator++(Fraction& f) {
    f.numerator = f.numerator + f.denominator;
    return f;
}
//Overloading Fraction++

Fraction operator++(Fraction& f, int) {
    Fraction ftemp = f;
    //Increment the original object, but return a copy of the original before it was incremented
    f.numerator = f.numerator + f.denominator;
    return ftemp;
}

//We'll do some other random overloads too because fuck it

Fraction operator+(Fraction f1, Fraction f2) {
    Fraction newFrac;
    f1.numerator = f1.numerator * f2.denominator;
    f2.numerator = f2.numerator * f1.denominator;
    newFrac.numerator = f1.numerator + f2.numerator;
    newFrac.denominator = f1.denominator * f2.denominator;
    newFrac.Simplify();
    return newFrac;
}
Fraction operator+(Fraction f1, int n1) {
    f1.numerator = f1.numerator + (n1 * f1.denominator);
    f1.Simplify();
    return f1;
}
Fraction operator+(int n1, Fraction f1) {
    //route to the original function.
    return f1 + n1;
}
bool operator==(Fraction f1, Fraction f2) {
    f1.Simplify();
    f2.Simplify();
    if(f1.numerator == f2.numerator && f1.denominator == f2.denominator) {
        return true;
    } else {
        return false;
    }
}
bool operator!=(Fraction f1, Fraction f2) {
    if(f1 == f2) {
        return false;
    } else {
        return true;
    }
}

int main() {
    //this is where various random things that don't actually need to be run will go.

    //Will work, however does not actually create an object since base of member reference will be a function.

    Circle c1();

    //Let's try this with a fraction.

    Fraction f1();
    //Doesn't actually work.
    Fraction f2;
    //Will work.
    Fraction f3(1, 5);
    //Also works.
    double f3Val = f3.Evaluate();
    //Since we used arguments in the initializer, it works.

    //Overloaded << operator example
    Fraction f4(5, 10);
    cout << f4 << endl; //Will display "5 / 10"
    //Overloaded increment functions.
    Fraction f5(1, 5);
    cout << ++f5 << endl; // Will display "6 / 5"
    cout << f5++ << endl; // Will display "6 / 5"
    cout << f5 << endl; //Will display "11 / 5", since last one was incremented after displaying it.
    Fraction f6(1, 5);
    Fraction f7(5, 10);
    f7.Simplify();
    cout << f7 << endl; // Will display "1 / 2"
    Fraction f8 = f6 + f7;
    cout << f8 << endl; // Will display "7 / 10"
    //Overloaded + sign for integers.
    f8 = f8 + 1;
    cout << f8 << endl; // Will display "17 / 10"
    f8 = 1 + f8;
    cout << f8 << endl; // Will display "27 / 10"
    Fraction f9(27, 10);
    Fraction f10(1, 2);
    if(f9 == f10) {
        cout << "yep" << endl;
    } else {
        cout << "nope" << endl;
    }
    if(f8 != f8) {
        cout << "yep" << endl;
    } else {
        cout << "nope" << endl;
    }

    //GO TO FILE STUDENTLIST.CPP FOR MORE, THIS FILE GOT TOO FUCKIN CROWDED
    return 0;
}

//makefiles:

/*
 * main: main.o
 *      g++ -o main main.o
 * main.o: main.cpp dependency.h
 *      g++ -c main.cpp
 * clean:
 *      rm -rf *.o main
 */