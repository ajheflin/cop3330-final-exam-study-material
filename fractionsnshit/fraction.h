//
// Created by AJ Heflin on 2019-12-10.
//
//Must include iostream so that compiler knows what ostreams are.
#include <iostream>

#ifndef MEGAPROGRAM_FRACTION_H
#define MEGAPROGRAM_FRACTION_H

class Fraction {
public:
    Fraction();
    Fraction(int n);
    Fraction(int num, int denom);
    double Evaluate();
    void Simplify();
private:
    int numerator;
    int denominator;
    //Friending << overload so that you can easily access the numerator and denominator private data.
    friend std::ostream& operator<<(std::ostream& s, Fraction f1);
    friend Fraction operator++(Fraction& f);
    friend Fraction operator++(Fraction& f, int);
    friend Fraction operator+(Fraction f1, Fraction f2);
    friend Fraction operator+(Fraction f1, int n1);
    friend Fraction operator+(int n1, Fraction f1);
    friend bool operator==(Fraction f1, Fraction f2);
};

#endif //MEGAPROGRAM_FRACTION_H
