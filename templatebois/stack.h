//
// Created by AJ Heflin on 2019-12-10.
//

#ifndef MEGAPROGRAMTEMPLATES_STACK_H
#define MEGAPROGRAMTEMPLATES_STACK_H

template<typename NODETYPE>
class Stack {
public:
    Stack(NODETYPE arr[], int s);
    void Print();
    void Sum();
private:
    NODETYPE *arrptr;
    int size;
};

#endif //MEGAPROGRAMTEMPLATES_STACK_H
