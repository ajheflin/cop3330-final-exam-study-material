#include <iostream>
#include "stack.h"

using namespace std;
//"NODETYPE" can actually be replaced with literally any word on the planet you can think of, just went with nodetype for continuity from assignment 7.
template<typename NODETYPE>
Stack<NODETYPE>::Stack(NODETYPE *arr, int s) {
    arrptr = new NODETYPE[s];
    size = s;
    for(int i = 0; i < size; i++) {
        arrptr[i] = arr[i];
    }
}
//I'll do it down here just to show it
//'typename' and 'class' are also interchangeable and have literally 0 difference, I will demonstrate that too.
template<class fuckit>
void Stack<fuckit>::Print() {
    for(int i = 0; i < size; i++) {
        cout << arrptr[i] << endl;
    }
}

int main() {
    int slist[5] = {1,2,3,4,5};
    Stack<int> s1(slist, 5);
    s1.Print();
    return 0;
}